package kade_hbase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hylanda.mrw.DataSandTableBuilder;
import com.hylanda.mrw.algorithm.Dictionary;
import com.hylanda.mrw.algorithm.Pattern;

public class relationCalc {
	public static class relationCalcMaper extends Mapper<Object,Text,Text,FloatWritable>{
		private Logger logger = LoggerFactory.getLogger(relationCalcMaper.class);
		private Set<String> highFreqSet = new HashSet<String>();
		private String highFreqDictPath = "/home/hadoop/zhangfang/dict/more30.txt"; 
		private Pattern<Integer> patA;
		private Pattern<Integer> patB;
		private int windowSize = 5;
		private bigSizeDictInitor bigDictIniter = null;
		private DataSandTableBuilder builder;
		
		public static Set<String> getHighFrequentWords(File file) throws IOException{
			Set<String> set = new HashSet<String>();
			FileInputStream FIreader = new FileInputStream(file);
			InputStreamReader read = new InputStreamReader(FIreader,"UTF-8");
			BufferedReader reader = new BufferedReader(read);
			String string = reader.readLine();
			
			while(string != null){
				set.add(string.split("\\t")[0]);
				string = reader.readLine();
			}
			reader.close();
			read.close();
			FIreader.close();
			return set;
		}
		
		public void setup(Context context) throws IOException,InterruptedException{
			super.setup(context);
			builder = new DataSandTableBuilder();
			bigDictIniter = new bigSizeDictInitor(FileSystem.get(new Configuration())); // hdfs
			bigDictIniter.startDict();
			highFreqSet = getHighFrequentWords(new File(highFreqDictPath));
			logger.info("高频词数："+highFreqSet.size());
		}
		
		public void cleanup(Context context) throws IOException,InterruptedException {
			super.cleanup(context);
			bigDictIniter.Unit();
		}
	    
	    private Text word = new Text();  
	    public void map(Object key, Text value, Context context ) throws IOException, InterruptedException {  

	    	Dictionary dict = bigDictIniter.GetDict();
	    	if (dict == null) {
				long lStart = System.currentTimeMillis();
				while (true) {
					Thread.sleep(20000);
					dict = bigDictIniter.GetDict();
					long ltime = System.currentTimeMillis() - lStart;
					if (dict != null) {
						builder.setDict(dict);
						break;
					} else {
						logger.info(String.format("Waiting Init for %ds...",
								ltime / 1000));
					}
					context.progress();
					if (ltime > 7200 * 1000) {
						throw new IOException("init time over 7200s");
					}
				}
			}
	    	String[] contents = value.toString().split("\t");
	    	if(contents.length==2){
	    		String[] sentences = contents[1].split("。&0|[.]&0|？&0|[?]&0|！&0|!&0");
		    	for(String sentence:sentences){
		    		String[] words = sentence.trim().split(" ");
				    FloatWritable distance = null;
				    String keyA, keyB;
				    String a=null;
				    String b=null;
				    try{
					    if(words.length>0){
					    	for (int i = 0; i < words.length; i++) {
								a = words[i];
								if(a.equals("")||a.equals("&")){
									continue;
								}
								String[] temp = a.split("&");
								String preA = temp[0];
								String subA = temp[1];
								if(preA.equals("")||subA.equals("")||subA.equals("32")||subA.equals("64")||subA.equals("38")||subA.equals("48")){
									continue;
								}
								if(highFreqSet.contains(preA)){
									patA = dict.get(preA);
									for (int j = 1; j <= windowSize && (i + j) < words.length; j++) {
										b = words[i + j];
										if(b.equals("")||b.equals("&")){
											continue;
										}
										temp = b.split("&");
										String preB = temp[0];
										String subB = temp[1];
										if(preB.equals("")||subB.equals("")||subB.equals("32")||subB.equals("64")||subB.equals("38")||subB.equals("48")){
											continue;
										}
										if(highFreqSet.contains(preB)){
										patB = dict.get(preB);
										distance = builder.value(patA, patB, j - i);
					
										if (patA == null) {
											keyA = "-1#" + a;
										} else {
											keyA = patA.getValue() + "#" + a;
										}
					
										if (patB == null) {
											keyB = "-1#" + b;
										} else {
											keyB = patB.getValue() + "#" + b;
										}
					
										word.set(keyA.trim() + "@" + keyB.trim());
										context.write(word,distance);
										word.set(keyB.trim() + "@" + keyA.trim());
										context.write(word,distance);
										}
									}
								}
							}	
					    }
				    }catch(Exception e){
				    	System.err.println("sentence:"+sentence);
				    	System.err.println("a:"+a);
				    	System.err.println("b:"+b);
				    	e.printStackTrace();
				    }
		    	}
	    	}		  
	    }  
	}
	
	public static class relationCalcReducer extends Reducer<Text, FloatWritable, Text, FloatWritable>{
		private Logger logger = LoggerFactory.getLogger(relationCalcReducer.class);
		private FloatWritable result = new FloatWritable();
		private float ratio;

		public void setup(Context context) throws IOException,InterruptedException{
			super.setup(context);
			Configuration conf = context.getConfiguration();
			ratio = Float.parseFloat(conf.get("ThreshRatio"));
		}
	    public void reduce(Text key, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException
	    {
	    	float baseThresh = (float)1000.0;
			float sum = 0.0F;
			for (FloatWritable val : values) {
				sum += val.get();
			}
			if(sum > baseThresh*ratio){
				result.set(sum);
				context.write(key, result);
			}
	    }
	}

	public static class relationCalcCombiner extends Reducer<Text, FloatWritable, Text, FloatWritable>{
		private Logger logger = LoggerFactory.getLogger(relationCalcCombiner.class);
		private FloatWritable result = new FloatWritable();

	    public void reduce(Text key, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException
	    {
			float sum = 0.0F;
			for (FloatWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(key, result);
	    }
	}
	
	public static void main(String[] args) throws Exception {
		System.setProperty("HADOOP_USER_NAME", "hadoop");
		Configuration conf = new Configuration();  
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length < 2) {  
	    	System.err.println(otherArgs[0]+"\n"+otherArgs[1]);  
			System.err.println("Usage: wordcount <in> <out>");  
			System.exit(2);  
		} 
	    conf.set("ThreshRatio", otherArgs[3]);
		conf.setBoolean("mapred.map.tasks.speculative.execution", false);
		conf.set("mapred.child.java.opts", "-Xmx4000m");
		conf.setInt("SpliteRegionSizeMB", 1024);
		conf.set("hltask.job.id",new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		conf.setBoolean("IsSpliteRegion", true);

		Job job = new Job(conf, "Relation_Caculation");
		job.setJarByClass(relationCalc.class);
		job.setMapperClass(relationCalcMaper.class);
		job.setCombinerClass(relationCalcCombiner.class);
	    job.setReducerClass(relationCalcReducer.class);
	    job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(FloatWritable.class);
		job.setNumReduceTasks(20);
		FileInputFormat.addInputPath(job, new Path(otherArgs[1]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[2]));
	    job.submit();
	    System.exit(job.waitForCompletion(true) ? 0 : 1); 

	}

}
