package kade_hbase;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

//import javassist.expr.NewArray;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.RandomRowFilter;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cpkf.yyjd.cjf.CJFBeanFactory;
import com.cpkf.yyjd.cjf.ChineseJF;
import com.hylanda.hlsegex.HLSegBigSize;
import com.hylanda.mrw.DataSandTableBuilder;
import com.hylanda.mrw.algorithm.Dictionary;
import com.hylanda.mrw.algorithm.Pattern;
import com.hylanda.segment.HLDictManager;
import com.hylanda.string.utils.TextProcess;

public class KadeSegment {

	public static String begintime;
	public static String endtime;
	public static String tableName;
	public static int sampleRatio = 7;
	public static boolean isSample = false;
	public static float acceptThreshHold=(float)0.125;
	public static class WordCountMap extends TableMapper<Text,Text> {

		private Logger logger = LoggerFactory.getLogger(WordCountMap.class);
		public final byte[] FAMILY_DOC = Bytes.toBytes("doc");
		public final byte[] FIELD_TITLE = Bytes.toBytes("title");
		public final byte[] FIELD_CONTENT = Bytes.toBytes("format_content");
		
		
		private bigSizeDictInitor bigDictIniter = null;
		private ArrayList<String> result;
		private HLDictManager instance;
		private FileSystem hdfs;
		private File stopwordspath;
		private File userstopwordspath;
		//private Path dictionary;
		private Map<String, Integer> stopwords = new HashMap<String, Integer>();
		//private Dictionary dict;
		private DataSandTableBuilder builder;
		private Text word;
		private int windowSize;
		private String a;
		private String b;
		private String keyA, keyB;
		private FloatWritable value;
		private Pattern<Integer> patA;
		private Pattern<Integer> patB;
		private Text KEY,VALUE;
		public static ChineseJF chinesdJF = CJFBeanFactory.getChineseJF();
		@Override
		public void setup(Context context) throws IOException,
				InterruptedException {
			super.setup(context);
			instance = HLDictManager.getInstance();
			hdfs = FileSystem.get(new Configuration());
			stopwordspath = new File("/home/hadoop/zhangfang/dict/stopwords.txt");
			userstopwordspath = new File("/home/hadoop/zhangfang/dict/userstopwords.txt");
			stopwords = TextProcess.loadstopwords(stopwordspath,userstopwordspath);
			word = new Text();
			KEY = new Text();
			VALUE = new Text();
			//dict = new Dictionary();
			builder = new DataSandTableBuilder();
			//builder.setDict(dict);

			bigDictIniter = new bigSizeDictInitor(hdfs);
			windowSize = 10;
			a = null;
			b = null;
			bigDictIniter.start();
			//dict.load(hdfs, dictionary);
			logger.info("setup finish!");
		}

		@Override
		public void cleanup(Context context) throws IOException,
				InterruptedException {
			super.cleanup(context);
			bigDictIniter.Unit();
		}

		public void map(ImmutableBytesWritable row, Result values,
				Context context) throws IOException, InterruptedException {
			// wait for dict init
			//if(isSample && Math.random()>=acceptThreshHold){
			//	return;
			//}
			HLSegBigSize bigDict = bigDictIniter.GetHLSegBigSize();
			Dictionary dict = bigDictIniter.GetDict();
			if (bigDict == null|| dict == null) {
				long lStart = System.currentTimeMillis();
				while (true) {
					Thread.sleep(20000);
					bigDict = bigDictIniter.GetHLSegBigSize();
					dict = bigDictIniter.GetDict();
					long ltime = System.currentTimeMillis() - lStart;
					if (bigDict != null && dict != null) {
						builder.setDict(dict);
						break;
					} else {
						logger.info(String.format("Waiting Init for %ds...",
								ltime / 1000));
					}
					context.progress();
					if (ltime > 7200 * 1000) {
						throw new IOException("init time over 7200s");
					}
				}
			}

			// byte[] byTitle = values.getValue(FAMILY_DOC,FIELD_TITLE);
			byte[] byContent = values.getValue(FAMILY_DOC, FIELD_CONTENT);
			byte[] byUrl = values.getValue(FAMILY_DOC,Bytes.toBytes("url"));
			String strText = new StringBuffer().append(
					Bytes.toString(byContent)).toString();
			String strUrl = new StringBuffer().append(
					Bytes.toString(byUrl)).toString();
			String seg_result = "";
			result = TextProcess.segment(strText, "userdict.txt", instance,
					bigDict, hdfs, stopwords, logger,chinesdJF);
			for (String s : result)
				seg_result += s + " ";
			KEY.set(strUrl);
			VALUE.set(seg_result);
			context.write(KEY,VALUE);
		}

	}

	public static class IntSumReducer extends
			Reducer< Text, Text, Text,Text> {
		private Logger logger = LoggerFactory.getLogger(IntSumReducer.class);
		private FloatWritable result = new FloatWritable();
		@Override
		protected void setup(Context context) throws IOException, InterruptedException
		{
			super.setup(context);
		}
		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException
		{
			super.cleanup(context);		
		}

		public void reduce( Text key,Iterable<Text> values,
				Context context) throws IOException, InterruptedException {

			float sum = 0;
			for (Text val : values) {
				context.write(key, val);
			}
		}

	}

	public static List<Scan> makeScan(Configuration conf) throws IOException,
			ParseException {
		List<Scan> list = new ArrayList<Scan>();

		int nScanCaching = 100;

		int nScanCachingWeibo = nScanCaching * 5;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//String tableName = "news_201503_ng,weibo_201503_ng";
		String[] tables = tableName.split(",");
		for (int j = 0; j < tables.length; j++) {
			// 妫�鏌cantable鏄惁閮藉瓨鍦�
			boolean bTableExist = true;
			try {
				HTable table = new HTable(conf, tables[j]);
			} catch (TableNotFoundException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			if (!bTableExist) {
				continue;
			}
			Scan scan = new Scan();
			scan.setCacheBlocks(false);
			Date beginTime = df.parse(begintime);
			Date endTime = df.parse(endtime);
			scan.setTimeRange(beginTime.getTime(), endTime.getTime());
			if (tables[j].startsWith("weibo")) {
				scan.setCaching(nScanCachingWeibo);
			} else {
				scan.setCaching(nScanCaching);
			}
			scan.addFamily(Bytes.toBytes("doc"));
			scan.setAttribute(Scan.SCAN_ATTRIBUTES_TABLE_NAME,
					Bytes.toBytes(tables[j]));
			scan.setFilter(new RandomRowFilter(acceptThreshHold));
			System.out.println("sample ratio:"+acceptThreshHold);
			list.add(scan);
		}
		return list;
	}


	public static void main(String[] args) throws IOException, ParseException {
		System.setProperty("HADOOP_USER_NAME", "hadoop");
		Configuration conf = HBaseConfiguration.create();
		String[] otherArgs = new GenericOptionsParser(conf, args)
				.getRemainingArgs();
		conf.setBoolean("mapred.map.tasks.speculative.execution", false);
		conf.set("mapred.job.tracker", "10.20.100.82:9001");
		conf.set("hbase.zookeeper.quorum", "10.20.100.82");
		conf.set("hbase.zookeeper.property.clientPort", "2181");
		conf.set("fs.default.name", "hdfs://10.20.100.82:9000");
		conf.set("mapred.child.java.opts", "-Xmx4000m");
		conf.setInt("SpliteRegionSizeMB", 1024);
		conf.set("hltask.job.id",new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		conf.setBoolean("IsSpliteRegion", true);
		try {

			Job job = new Job(conf, "kade");
			FileOutputFormat.setOutputPath(job, new Path("/user/hadoop/"+otherArgs[0]));
			tableName = otherArgs[1];
			begintime = otherArgs[2]+" "+otherArgs[3];
			endtime = otherArgs[4]+" "+otherArgs[5];
			//sampleRatio = Integer.parseInt(otherArgs[6]);
			//acceptThreshHold = ((float)1.0)/sampleRatio;
			acceptThreshHold = Float.parseFloat(otherArgs[6]);
			//if(sampleRatio>1){
			//	isSample = true;
			//	}
			List<Scan> scans = makeScan(conf);
			job.setJarByClass(KadeSegment.class);
			TableMapReduceUtil.initTableMapperJob(scans, WordCountMap.class,
					 Text.class,Text.class, job);
			//job.setInputFormatClass(HylandaMultiTableInputFormat.class);
			//job.setReducerClass(IntSumReducer.class);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(Text.class);
			job.setNumReduceTasks(0);
			job.getConfiguration().setInt("mapred.task.timeout", 6000000); 
			
			job.submit();
			System.exit(job.waitForCompletion(true) ? 0 : 1); 
		} catch (ClassNotFoundException ex1) {
			ex1.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(0);

	}

}
