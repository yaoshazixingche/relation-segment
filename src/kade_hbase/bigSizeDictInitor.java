package kade_hbase;
import java.io.File;
import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hylanda.hlsegex.HLSegBigSize;
import com.hylanda.mrw.algorithm.Dictionary;


public class bigSizeDictInitor {
	private Logger logger = LoggerFactory.getLogger(bigSizeDictInitor.class);
	public class InitThread implements Runnable{
		bigSizeDictInitor _mgr = null;
		public InitThread(bigSizeDictInitor mgr){
			_mgr = mgr;
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (_mgr != null) {
				_mgr.Init();
			}
		}		
	}
	public class InitDictThread implements Runnable{
		bigSizeDictInitor _mgr = null;
		public InitDictThread(bigSizeDictInitor mgr){
			_mgr = mgr;
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (_mgr != null) {
				_mgr.InitDict();
			}
		}		
	}
	
	private HLSegBigSize segBigSize = null;
	public Dictionary _dict = null;
	private Thread thx = null;
	private Thread thx2 = null;

	private FileSystem _hdfs = null;;
	public bigSizeDictInitor(FileSystem hdfs){
		_hdfs = hdfs;
	}
	public void start(){
		thx = new Thread(new InitThread(this));
		thx2 = new Thread(new InitDictThread(this));
		thx.setDaemon(true);
		thx2.setDaemon(true);		
		thx.start();
		thx2.start();
		logger.info("Initer Thread Started");
	}
	//0706 only for dict.dat 
	public void startDict(){
		thx = new Thread(new InitDictThread(this));
		thx.setDaemon(true);		
		thx.start();
		logger.info("Dict Initer Thread Started");
	}
	
	public void Init(){
		logger.info("Start Init BigDict...");
		HLSegBigSize dict = new HLSegBigSize();
		if (!dict.init("/home/hadoop/zhangfang/dict/bigSizeWords.dat")){
			System.out.println("hlseg load failed!");

			logger.error("Init BigDict Failed");
		}
		else{
			synchronized (this) {
				segBigSize = dict;	
			}
			System.out.println("hlseg load OK!");
			logger.info("Init BigDict Success");
		}
	}
	
	public void InitDict(){
		logger.info("Start InitDict...");
		Dictionary dict = new Dictionary();
		try {
			dict.load(new File("/home/hadoop/zhangfang/dict/dict.dat"));
			synchronized (this) {
				_dict = dict;	
			}
			logger.info("InitDict Success");
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("InitDict Failed"+e.toString());
		}

		
	}
	
	public void Unit(){
		synchronized (this) {
			if (segBigSize != null) {
				segBigSize.uninit();	
			}
		}
	}
	
	public HLSegBigSize GetHLSegBigSize(){
		HLSegBigSize ret = null;
		synchronized(this){
			ret = segBigSize;
		}
		return ret;		
	}

	public Dictionary GetDict(){
		Dictionary ret = null;
		synchronized(this){
			ret = _dict;
		}
		return ret;		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
