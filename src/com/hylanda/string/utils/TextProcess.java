package com.hylanda.string.utils;

import com.cpkf.yyjd.cjf.ChineseJF;
import com.hylanda.hlsegex.HLSegBigSize;
import com.hylanda.hlsegex.SegmentWordResult;
import com.hylanda.segment.HLDictManager;
import com.hylanda.word.heat.HLContentHandler;
import com.hylanda.word.heat.HLErrorHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLReaderFactory;

public class TextProcess
{
  public static int N_PRINT_CNT = 0;

  public static String getFormatText_SAX(String strHtml)
  {
    if ((strHtml == null) || (strHtml.length() == 0))
      return "";
    ContentHandler content = new HLContentHandler();
    try
    {
      org.xml.sax.XMLReader reader = null;
      reader = XMLReaderFactory.createXMLReader("org.htmlparser.sax.XMLReader");
      reader.setContentHandler(content);
      ErrorHandler errors = new HLErrorHandler();
      reader.setErrorHandler(errors);

      org.htmlparser.sax.XMLReader htmReader = (org.htmlparser.sax.XMLReader)reader;
      org.htmlparser.scanners.ScriptScanner.STRICT = false;
      org.htmlparser.lexer.Lexer.STRICT_REMARKS = false;
      htmReader.parse(strHtml, "utf-8");
    }
    catch (SAXException e1)
    {
      e1.printStackTrace();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    return ((HLContentHandler)content).m_sbText.toString();
  }

	public static Map<String,Integer> loadstopwords (File stopword, File userstopword) throws IOException{
		Map<String,Integer> stopwords =new HashMap<String,Integer>();
		if(!stopword.exists()||stopword.isDirectory())
			System.out.println(stopword.getAbsolutePath()+"不存在!");
		else{
			FileInputStream FIreader = new FileInputStream(stopword);
			InputStreamReader read = new InputStreamReader(FIreader,"UTF-8");
			BufferedReader reader = new BufferedReader(read);
			String s = reader.readLine();
			while(s!=null){
				stopwords.put(s.trim(), 1);
				s = reader.readLine();
			}
			reader.close();
			read.close();
			FIreader.close();
		}
		if(!userstopword.exists()||userstopword.isDirectory())
			System.out.println(userstopword.getAbsolutePath()+"不存在!");
		else{
			FileInputStream FIreader = new FileInputStream(userstopword);
			InputStreamReader read = new InputStreamReader(FIreader,"UTF-8");
			BufferedReader reader = new BufferedReader(read);
			String s = reader.readLine();
			while(s!=null){
				stopwords.put(s.trim(), 1);
				s = reader.readLine();
			}
			reader.close();
			read.close();
			FIreader.close();
		}
		return stopwords;
	}

  public static String Flag(int flags, int flagsEx) {
    if ((flagsEx == 1) || (flagsEx == 2) || (flagsEx == 4) || (flagsEx == 8) || (flagsEx == 16) || (flagsEx == 32) || (flagsEx == 64))
      return ""+flagsEx;
    if (flags == 32) return "33";
    if (flags == 256) return "34";
    if (flags == 512) return "35";
    if (flags == 1024) return "36";
    if (flags == 4096) return "37";
    if (flags == 8192) return "38";
    if (flags == 32768) return "39";
    if (flags == 65536) return "40";
    if (flags == 262144) return "41";
    if (flags == 524288) return "42";
    if (flags == 8388608) return "43";
    if (flags == 16777216) return "44";
    if (flags == 33554432) return "45";
    if (flags == 67108864) return "46";
    if (flags == 134217728) return "47";
    if (flags == 268435456) return "48";
    if (flags == 536870912) return "49";
    if (flags == 1073741824) return "50";
    if (flags == 1048576) return "51";

    return ""+flagsEx;
  }
  public static ArrayList<String> segment(String content, String userdicpath, HLDictManager instance, HLSegBigSize segBigSize, FileSystem hdfs, Map<String, Integer> stopwords, Logger logger, ChineseJF chinesdJF) throws IOException {
    ArrayList StringList = new ArrayList();

    String string = null;
    //词性过滤
    Set<String> flagFilterList = new HashSet<String>();
	  String filterString = "128,32,64,35,38,39,40,41,48";
	  for(String item:filterString.split(",")){
		  if(!item.equals("")){
			  flagFilterList.add(item);
		  }
	  }
	  
    if (content != null) {
      content = content.replace("\r\n", " ").replace("\n", " ").replace("\r", " ");
      SegmentWordResult[] result = segBigSize.segmentBigSize(getFormatText_SAX(chinesdJF.chineseFan2Jan(content)));
      if (result != null) {
        for (int i = 0; i < result.length; i++) {
          string = result[i].word;

          if ((string == null) || (string.trim().length() == 0)||(string.trim().length()>8)) {
            continue;
          }
          if(!stopwords.containsKey(string)&&result[i].flags != 0x800){
        	  String flag = Flag(result[i].flags,result[i].flagsEx);
        	  if(!flagFilterList.contains(flag)){
        		  StringList.add(string+"&"+flag);
        	  }
          }
        }

      }

    }

    return StringList;
  }
}