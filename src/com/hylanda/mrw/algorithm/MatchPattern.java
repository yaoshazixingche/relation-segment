package com.hylanda.mrw.algorithm;


public class MatchPattern<T> implements Comparable<MatchPattern<T>>{

	public MatchPattern(Pattern<T> pattern, int offset, int len) {
		super();
		this.pattern = pattern;
		this.offset = offset;
		this.len = len;
	}
	
	public Pattern<T> pattern;
	public int offset;
	public int len;
	public void copyFrom(MatchPattern<T> other) {
		this.pattern = other.pattern;
		this.offset = other.offset;
		this.len = other.len;
	}
	
	@Override
	public  int hashCode() {
		return (pattern.hashCode() * 31 +  offset) * 31  + len;
	}
	
	@Override
	public  boolean equals(Object p) {
		if (this == p) {
			return true;
		}
		 if (p instanceof MatchPattern) {
			 MatchPattern<T> o = (MatchPattern<T>)p;
			 return pattern.equals(o.pattern) 
					 && offset == o.offset 
					 && len == o.len;
		 }
		 return false;
	}
	
	@Override 
	public String toString() {
		return "pattern=" + pattern +  ",offset=" + offset + ",len=" + len;
	}
	
	@Override 
	public int compareTo(MatchPattern<T> other) {
		if(this == other)
			return 0;
		int hashCode1 = offset * 100;
		int hashCode2 = other.offset * 100 + other.len;
		if(hashCode1 < hashCode2) {
			return -1;
		} else if(hashCode1 > hashCode2) {
			return 1;
		}
		return pattern.compareTo(other.pattern);
	}
	
}
