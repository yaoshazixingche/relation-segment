package com.hylanda.mrw.algorithm;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class DictItem implements DataSerializable{
	private int id;
	private String name;
	private String level1; 
	private String level2;
	private String level3;
	private String source;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLevel1() {
		return level1;
	}
	public void setLevel1(String level1) {
		this.level1 = level1;
	}
	public String getLevel2() {
		return level2;
	}
	public void setLevel2(String level2) {
		this.level2 = level2;
	}
	public String getLevel3() {
		return level3;
	}
	public void setLevel3(String level3) {
		this.level3 = level3;
	}
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	@Override
	public void load(DataInput stream) throws IOException {
		id = stream.readInt();
		name = stream.readUTF();
		level1 = stream.readUTF();
		level2 = stream.readUTF();
		level3 = stream.readUTF();
		source = stream.readUTF();
		
	}
	@Override
	public void save(DataOutput stream) throws IOException {
		normalize();
		stream.writeInt(id);
		stream.writeUTF(name);
		stream.writeUTF(level1);
		stream.writeUTF(level2);
		stream.writeUTF(level3);
		stream.writeUTF(source);
		
	}
	
	private void normalize() {
		if(name == null)
			name = "";
		if(level1 == null)
			level1 = "";
		if(level2 == null)
			level2 = "";
		if(level3 == null)
			level3 = "";
		if(source == null)
			source = "";
	}
	
	public String toString() {
		normalize();
		return id + "," + name + "," + level1 + "," + level2 + "," + level3 +  "," + source;
	}
}
