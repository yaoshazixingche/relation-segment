package com.hylanda.mrw.algorithm;

/*** 替代 String 的类， 注意这是避免使用 String 时内存总在复制从而效率低。
 * 牺牲了一定的安全性
 * @author zhouzusheng
 * 
 */
public class CharBuffer implements Comparable<CharBuffer>{

	private char[] buffer;
	private int length;
	private int hash;

	public CharBuffer(char[] buffer) {
		this.buffer = buffer;
		length = buffer.length;
		hash = 0;
	}

	public CharBuffer(char[] buffer, int size) {
		this.buffer = buffer;
		length = size;
		hash = 0;
	}

	public int size() {
		return length;
	}

	public String toString() {
		return new String(buffer, 0, length);
	}

	/**
	 * 获取字符缓冲区，牺牲安全性，不做拷贝， 使用的时候不要修改 buffer内容
	 * @return
	 */
	public char[] getBuffer() {
		return buffer;
	}
	
	public CharBuffer subBuffer(int off, int len) {
		if(off >= length || (off + len) > length) {
			return null;
		}
		char[] dest = new char[len];
		System.arraycopy(buffer,  off,  dest,  0, len);
		return new CharBuffer(dest);
	}

	public void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin) {
		if (srcBegin < 0)
			throw new StringIndexOutOfBoundsException(srcBegin);
		if (srcEnd < 0 || srcEnd > this.length)
			throw new StringIndexOutOfBoundsException(srcEnd);
		if (srcBegin > srcEnd)
			throw new StringIndexOutOfBoundsException("srcBegin > srcEnd");
		System.arraycopy(this.buffer, srcBegin, dst, dstBegin, srcEnd
				- srcBegin);
	}

	@Override
	public int hashCode() {
		if (hash == 0 && length > 0) {
			int i = 0;
			for (int l = 0; l < length; ++l) {
				i = 31 * i + buffer[l];
			}
			hash = i;
		}
		return hash;
	}

	@Override
	public boolean equals(Object p) {
		if (this == p) {
			return true;
		}
		if (p instanceof CharBuffer) {
			CharBuffer o = (CharBuffer) p;
			int i = this.length;
			if (i == o.length) {
				char[] arrayOfChar1 = this.buffer;
				char[] arrayOfChar2 = o.buffer;
				int j = 0;
				int k = 0;
				while (i-- != 0) {
					if (arrayOfChar1[(j++)] != arrayOfChar2[(k++)])
						return false;
				}
				return true;
			}
		}
		return false;
	}

	@Override 
	public int compareTo(CharBuffer other) {
		int i = length;
		int j = other.length;
		int k = Math.min(i, j);
		char[] arrayOfChar1 = this.buffer;
		char[] arrayOfChar2 = other.buffer;
		int l = 0;
		int i1 = 0;
		int i2;
		int i3;
		if (l == i1) {
			i2 = l;
			i3 = k + l;
			while (i2 < i3) {
				int i4 = arrayOfChar1[i2];
				int i5 = arrayOfChar2[i2];
				if (i4 != i5) {
					return (i4 - i5);
				}
				++i2;
			}
		} else {
			while (k-- != 0) {
				i2 = arrayOfChar1[(l++)];
				i3 = arrayOfChar2[(i1++)];
				if (i2 != i3) {
					return (i2 - i3);
				}
			}
		}
		return (i - j);
	}

	public static CharBuffer valueOf(String str) {
		return new CharBuffer(str.toCharArray());
	}
}
