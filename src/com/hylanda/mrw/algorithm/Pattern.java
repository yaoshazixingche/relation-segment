package com.hylanda.mrw.algorithm;

public class Pattern<T> implements Comparable<Pattern<T>> {
	public final CharBuffer word;
	public final T value;
	public Pattern(char[] src, T value) {
		this.word = new CharBuffer(src);
		this.value = value;
	}
	
	public Pattern(CharBuffer src, T value) {
		this.word = src;
		this.value = value;
	}
	
	public Pattern(String buffer, T id) {
		this.word = CharBuffer.valueOf(buffer);
		this.value = id;
	}
	
	public CharBuffer getWord() {
		return word;
	}
	
	public String getWordString() {
		return word.toString();
	}
	
	public T getValue() {
		return value;
	}
	
	public boolean findMatchInString(String str) {
		int len = word.size();
		if(len > str.length())
			return false;
		
		char[] buffer = word.getBuffer();
		int beginIndex = str.length() - len;
		for(int i = 0; i < len; i++) {
			char c1 = buffer[i];
			char c2 = str.charAt(i + beginIndex);
			if(c1 != c2)
				return false;
		}
		return true;
	}
	
	public boolean findMatchInString(char[] src, int begin, int count) {
		int len = word.size();
		if (len > count)  			
			return false;  		
		
		int beginIndex = begin + count - len;
		char[] buffer = word.getBuffer();
		for(int i = 0; i< len; i++) {
			char c1 = buffer[i];
			char c2 = src[beginIndex + i];
			if(c1 != c2)
				return false;
		}
		return true;
	}
	
	
	@Override
	public  int hashCode() {
		return word.hashCode() * 31 + value.hashCode();
	}
	
	@Override
	public  boolean equals(Object p) {
		if (this == p) {
			return true;
		}
		 if (p instanceof Pattern) {
			 Pattern<T> o = (Pattern<T>)p;
			 return word.equals(o.word) && value.equals(o.value);
		 }
		 return false;
	}
	
	@Override 
	public int compareTo(Pattern<T> other) {
		int code = word.compareTo(other.word);
		if(code != 0)
			return code;
		if(value instanceof Comparable) {
			return ((Comparable<T>)value).compareTo(other.value);
		}
		return 0;
	}
	
	@Override 
	public String toString() {
		return word.toString() + ",id=" + value;
	}
}
