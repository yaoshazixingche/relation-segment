package com.hylanda.mrw.algorithm.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TextFileUtils {
	
	public static int load(String file, FileLoading loading) throws IOException {
		return load(file, "UTF-8", loading);
	}
	
	public static int load(String file, String Encoding,  FileLoading loading) throws IOException{
		int n = 0;
		File unitFile = new File(file);
		InputStream fin = null;
		if(unitFile.exists()) {
			fin = new FileInputStream(unitFile);
			n = load(fin, Encoding, loading);
			fin.close();
		}
		return n;
	}
	
	public static int load(InputStream fin, FileLoading loading)
			throws IOException {
		return load(fin, "UTF-8", loading);
	}
	
	public static int load(InputStream fin, String Encoding, FileLoading loading)
			throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new BufferedInputStream(fin), Encoding));
		String line = null;
		int n = 0;
		while ((line = br.readLine()) != null) {
			if ( line.startsWith("#")) {
				continue;
			}
			n++;
			loading.row(line, n);
		}
		return n;
	}
}
