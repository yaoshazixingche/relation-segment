package com.hylanda.mrw.algorithm.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class CollectionUtils {
	public static interface MapHelper<K, V, O> {
		K getKey(O item);
		V getValue(O item);
	}
	
	private static enum COMPARESULT {
		UNKNOWN, EQUAL, LESS, THAN,
	};

	/**
	 * 求集合的交集
	 * 
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static <T extends Comparable<T>> void intersection(Collection<T> a,
			Collection<T> b, boolean skipSort, Collection<T> dest) {
		if (a.isEmpty() || b.isEmpty()) {
			return;
		}

		Class<?> type = a.iterator().next().getClass();

		@SuppressWarnings("unchecked")
		T[] left = (T[]) java.lang.reflect.Array.newInstance(type, a.size());

		@SuppressWarnings("unchecked")
		T[] right = (T[]) java.lang.reflect.Array.newInstance(type, b.size());

		a.toArray(left);
		b.toArray(right);
		intersection(left, right, skipSort, dest);
	}

	/**
	 * 求集合的交, 针对 MAP 类型
	 * 
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static <K extends Comparable<K>, V> void intersection(Map<K, V> a,
			Map<K, V> b, boolean skipSort, Map<K, V> dest) {
		if (a.isEmpty() || b.isEmpty()) {
			return;
		}
		Set<Map.Entry<K, V>> entrys = a.entrySet();
		Class<?> type = entrys.iterator().next().getClass();

		@SuppressWarnings("unchecked")
		Map.Entry<K, V>[] left = (Map.Entry<K, V>[]) java.lang.reflect.Array
				.newInstance(type, a.size());

		@SuppressWarnings("unchecked")
		Map.Entry<K, V>[] right = (Map.Entry<K, V>[]) java.lang.reflect.Array
				.newInstance(type, b.size());

		entrys.toArray(left);
		b.entrySet().toArray(right);

		Comparator<Map.Entry<K, V>> compator = new Comparator<Map.Entry<K, V>>() {

			@Override
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		};

		if (!skipSort) {
			Arrays.sort(left, compator);
			Arrays.sort(right, compator);
		}

		// 优化
		if (compator.compare(left[0], right[right.length - 1]) > 0
				|| compator.compare(right[0], left[left.length - 1]) > 0) {
			return;
		}

		for (int indexA = 0, lenA = left.length, indexB = 0, lenB = right.length; indexA < lenA
				&& indexB < lenB;) {
			Map.Entry<K, V> first = left[indexA];
			Map.Entry<K, V> second = right[indexB];
			int code = compator.compare(first, second);

			if (code == 0) {
				dest.put(first.getKey(), first.getValue());
				indexA++;
				indexB++;
			} else if (code < 0) {
				indexA++;
			} else {
				indexB++;
			}
		}
	}
	
	/**
	 * 求集合的交集
	 * 
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static <T extends Comparable<T>> void intersection(T[] a, T[] b,
			boolean skipSort, Collection<T> dest) {
		if (!skipSort) {
			Arrays.sort(a);
			Arrays.sort(b);
		}
		// 优化
		if (a[0].compareTo(b[b.length - 1]) > 0
				|| b[0].compareTo(a[a.length - 1]) > 0) {
			return;
		}
		for (int indexA = 0, lenA = a.length, indexB = 0, lenB = b.length; indexA < lenA
				&& indexB < lenB;) {
			T first = a[indexA];
			T second = b[indexB];
			int code = first.compareTo(second);

			if (code == 0) {
				dest.add(first);
				indexA++;
				indexB++;
			} else if (code < 0) {
				indexA++;
			} else {
				indexB++;
			}
		}
	}
	
	/**
	 * 求 a b 的并集
	 * @param a
	 * @param b
	 * @param strip： 是否消除相同的项
	 * @param dest
	 */
	public static <T> void union(Collection<T> a,
			Collection<T> b, boolean strip, Collection<T> dest) {
		if(strip) {
			Set<T> tmp = new HashSet<T>();
			for(T item : a) {
				if(tmp.add(item)) {
					dest.add(item);
				}
			}
			for(T item : b) {
				if(tmp.add(item)) {
					dest.add(item);
				}
			}
			tmp.clear();
		} else {
			dest.addAll(a);
			dest.addAll(b);
		}
	}
	
	/**
	 * 求 a b 的并集
	 * @param a
	 * @param b
	 * @param strip： 是否消除相同的项
	 * @param dest
	 */
	public static <K , V> void union(Map<K, V> a,
			Map<K, V> b,  Map<K, V> dest) {
		
		dest.putAll(a);
		dest.putAll(b);
	}
	
	/**
	 * 求 a b 的并集
	 * @param a
	 * @param b
	 * @param strip： 是否消除相同的项
	 * @param dest
	 */
	public static <T> void union(T[] a, T[] b,
			boolean strip, Collection<T> dest) {
		if(strip) {
			Set<T> tmp = new HashSet<T>();
			for(T item : a) {
				if(tmp.add(item)) {
					dest.add(item);
				}
			}
			for(T item : b) {
				if(tmp.add(item)) {
					dest.add(item);
				}
			}
			tmp.clear();
		} else {
			for(T item : a) {
				dest.add(item);
			}
			for(T item : b) {
				dest.add(item);
			}
		}
	}


	/**
	 * 求集合的差集 a -b
	 * 
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static <T extends Comparable<T>> void diff(Collection<T> a,
			Collection<T> b, boolean skipSort, Collection<T> dest) {
		if (a.isEmpty()) {
			return;
		}
		if (b.isEmpty()) {
			dest.addAll(a);
			return;
		}
		Class<?> type = a.iterator().next().getClass();

		@SuppressWarnings("unchecked")
		T[] left = (T[]) java.lang.reflect.Array.newInstance(type, a.size());

		@SuppressWarnings("unchecked")
		T[] right = (T[]) java.lang.reflect.Array.newInstance(type, b.size());

		a.toArray(left);
		b.toArray(right);
		diff(left, right, skipSort, dest);
	}
	

	
	
	

	/**
	 * 求集合的差集 a-b
	 * 
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static <T extends Comparable<T>> void diff(T[] a, T[] b,
			boolean skipSort, Collection<T> dest) {
		if (!skipSort) {
			Arrays.sort(a);
			Arrays.sort(b);
		}
		// 优化
		if (a[0].compareTo(b[b.length - 1]) > 0
				|| b[0].compareTo(a[a.length - 1]) > 0) {
			for (T item : a)
				dest.add(item);
			return;
		}

		//记录上一个匹配结果
		COMPARESULT lastResult = COMPARESULT.UNKNOWN;
		//记录上上一个匹配结果
		COMPARESULT lastLastResult = COMPARESULT.UNKNOWN;
		int lastA = -1;
		final int lenA = a.length;
		for (int indexA = 0,  indexB = 0, lenB = b.length; indexA < lenA
				&& indexB < lenB;) {
			T first = a[indexA];
			T second = b[indexB];
			int code = first.compareTo(second);

			if (code == 0) {
				lastLastResult = lastResult;
				lastResult = COMPARESULT.EQUAL;
				//把相等的值跳过
				while(indexA  < lenA && a[indexA].compareTo(first) == 0) {
					lastA = indexA++;
				}
				//把相等的值跳过
				while(indexB  < lenB && b[indexB].compareTo(first) == 0) {
					indexB++;
				}
				continue;
			} else if (code < 0) {
				if(lastResult == COMPARESULT.LESS) {
					//将前一个待决定的项放到结果中
					if (lastLastResult == COMPARESULT.LESS)
						dest.add(a[lastA]);
				} else {
					//这种情况下，一定可以放到结果中
					dest.add(first);
				}

				lastLastResult = lastResult;
				lastResult = COMPARESULT.LESS;
				lastA = indexA++;

			} else {
				//待决定
				lastLastResult = lastResult;
				lastResult = COMPARESULT.THAN;
				lastA = indexA;
				indexB++;
			}
		}

		// 末尾一个要再加以判断
		if (lastA < lenA) {
			if (lastResult == COMPARESULT.THAN
					|| (lastResult == COMPARESULT.LESS && lastLastResult == COMPARESULT.LESS))
				dest.add(a[lastA]);
		}

		//a 中剩余的已经要加到结果中
		for (int indexA = lastA + 1; indexA < lenA; indexA++) {
			dest.add(a[indexA]);
		}

	}

	/**
	 * 求集合的差 a-b, 针对 MAP 类型
	 * 
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static <K extends Comparable<K>, V> void diff(Map<K, V> a,
			Map<K, V> b, boolean skipSort, Map<K, V> dest) {
		if (a.isEmpty()) {
			return;
		}
		if (b.isEmpty()) {
			dest.putAll(a);
			return;
		}

		Set<Map.Entry<K, V>> entrys = a.entrySet();
		Class<?> type = entrys.iterator().next().getClass();

		@SuppressWarnings("unchecked")
		Map.Entry<K, V>[] left = (Map.Entry<K, V>[]) java.lang.reflect.Array
				.newInstance(type, a.size());

		@SuppressWarnings("unchecked")
		Map.Entry<K, V>[] right = (Map.Entry<K, V>[]) java.lang.reflect.Array
				.newInstance(type, b.size());

		entrys.toArray(left);
		b.entrySet().toArray(right);

		Comparator<Map.Entry<K, V>> compator = new Comparator<Map.Entry<K, V>>() {

			@Override
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		};

		if (!skipSort) {
			Arrays.sort(left, compator);
			Arrays.sort(right, compator);
		}

		// 优化
		if (compator.compare(left[0], right[right.length - 1]) > 0
				|| compator.compare(right[0], left[left.length - 1]) > 0) {
			dest.putAll(a);
		}
		
		//记录上一个匹配结果
		COMPARESULT lastResult = COMPARESULT.UNKNOWN;
		//记录上上一个匹配结果
		COMPARESULT lastLastResult = COMPARESULT.UNKNOWN;
		int lastA = -1;
		final int lenA = left.length;

		for (int indexA = 0, indexB = 0, lenB = right.length; indexA < lenA
				&& indexB < lenB;) {
			Map.Entry<K, V> first = left[indexA];
			Map.Entry<K, V> second = right[indexB];
			int code = compator.compare(first, second);

			if (code == 0) {
				lastLastResult = lastResult;
				lastResult = COMPARESULT.EQUAL;
				//把相等的值跳过
				while(indexA  < lenA && compator.compare(left[indexA], first) == 0) {
					lastA = indexA++;
				}
				//把相等的值跳过
				while(indexB  < lenB && compator.compare(right[indexB], first) == 0) {
					indexB++;
				}
				continue;
			} else if (code < 0) {
				if(lastResult == COMPARESULT.LESS) {
					//将前一个待决定的项放到结果中
					if (lastLastResult == COMPARESULT.LESS) {
						first = left[lastA];
						dest.put(first.getKey(), first.getValue());
					}
				} else {
					//这种情况下，一定可以放到结果中
					dest.put(first.getKey(), first.getValue());
				}

				lastLastResult = lastResult;
				lastResult = COMPARESULT.LESS;
				lastA = indexA++;

			} else {
				//待决定
				lastLastResult = lastResult;
				lastResult = COMPARESULT.THAN;
				lastA = indexA;
				indexB++;
			}
		}
		// 末尾一个要再加以判断
		if (lastA < lenA) {
			if (lastResult == COMPARESULT.THAN
					|| (lastResult == COMPARESULT.LESS && lastLastResult == COMPARESULT.LESS)) {
				Map.Entry<K, V> first = left[lastA];
				dest.put(first.getKey(), first.getValue());
			}
		}

		for (int indexA = lastA + 1; indexA < lenA; indexA++) {
			Map.Entry<K, V> first = left[indexA];
			dest.put(first.getKey(), first.getValue());
		}

	}
	
	/**
	 *判断集合是否有交集 
	 * @param a
	 * @param b
	 * @param skipSort
	 */
	public static boolean isIntersection(int[] a, int[] b, boolean skipSort) {
		if (!skipSort) {
			Arrays.sort(a);
			Arrays.sort(b);
		}
		// 优化
		if (a[0] > b[b.length - 1] || b[0] > a[a.length - 1]) {
			return false;
		}
		for (int indexA = 0, lenA = a.length, indexB = 0, lenB = b.length; indexA < lenA
				&& indexB < lenB;) {
			int first = a[indexA];
			int second = b[indexB];

			if (first == second) {
				return true;
			} else if (first < second) {
				indexA++;
			} else {
				indexB++;
			}
		}
		return false;
	}
	
	/**
	 * 求集合的交，针对int[] 数组单独写一份，因为java 语言的限制，无法省去
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static void intersection(int[] a, int[] b, boolean skipSort,
			Collection<Integer> dest) {
		if (!skipSort) {
			Arrays.sort(a);
			Arrays.sort(b);
		}
		// 优化
		if (a[0] > b[b.length - 1] || b[0] > a[a.length - 1]) {
			return;
		}
		for (int indexA = 0, lenA = a.length, indexB = 0, lenB = b.length; indexA < lenA
				&& indexB < lenB;) {
			int first = a[indexA];
			int second = b[indexB];

			if (first == second) {
				dest.add(first);
				indexA++;
				indexB++;
			} else if (first < second) {
				indexA++;
			} else {
				indexB++;
			}
		}
	}

	/**
	 * 求集合的交，针对long[] 数组单独写一份，因为java 语言的限制，无法省去
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static void intersection(long[] a, long[] b, boolean skipSort,
			Collection<Long> dest) {
		if (!skipSort) {
			Arrays.sort(a);
			Arrays.sort(b);
		}
		// 优化
		if (a[0] > b[b.length - 1] || b[0] > a[a.length - 1]) {
			return;
		}
		for (int indexA = 0, lenA = a.length, indexB = 0, lenB = b.length; indexA < lenA
				&& indexB < lenB;) {
			long first = a[indexA];
			long second = b[indexB];

			if (first == second) {
				dest.add(first);
				indexA++;
				indexB++;
			} else if (first < second) {
				indexA++;
			} else {
				indexB++;
			}
		}
	}
	
	/**
	 * 求 a b 的并集, 针对 int[]
	 * @param a
	 * @param b
	 * @param strip： 是否消除相同的项
	 * @param dest
	 */
	public static <T> void union(int[] a, int[] b,
			boolean strip, Collection<Integer> dest) {
		if(strip) {
			Set<Integer> tmp = new HashSet<Integer>();
			for(Integer item : a) {
				if(tmp.add(item)) {
					dest.add(item);
				}
			}
			for(Integer item : b) {
				if(tmp.add(item)) {
					dest.add(item);
				}
			}
			tmp.clear();
		} else {
			for(Integer item : a) {
				dest.add(item);
			}
			for(Integer item : b) {
				dest.add(item);
			}
		}
	}
	
	/**
	 * 求 a b 的并集, 针对 long[]
	 * @param a
	 * @param b
	 * @param strip： 是否消除相同的项
	 * @param dest
	 */
	public static <T> void union(long[] a, long[] b,
			boolean strip, Collection<Long> dest) {
		if(strip) {
			Set<Long> tmp = new HashSet<Long>();
			for(Long item : a) {
				if(tmp.add(item)) {
					dest.add(item);
				}
			}
			for(Long item : b) {
				if(tmp.add(item)) {
					dest.add(item);
				}
			}
			tmp.clear();
		} else {
			for(Long item : a) {
				dest.add(item);
			}
			for(Long item : b) {
				dest.add(item);
			}
		}
	}

	/**
	 * 求集合的差集 a-b,针对int[] 数组单独写一份，因为java 语言的限制，无法省去
	 * 
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static void diff(int[] a, int[] b, boolean skipSort,
			Collection<Integer> dest) {
		if (!skipSort) {
			Arrays.sort(a);
			Arrays.sort(b);
		}
		// 优化
		if (a[0] > b[b.length - 1] || b[0] > a[a.length - 1]) {
			for (int item : a)
				dest.add(item);
			return;
		}

		COMPARESULT lastResult = COMPARESULT.UNKNOWN;
		COMPARESULT lastLastResult = COMPARESULT.UNKNOWN;
		int lastA = -1;
		final int lenA = a.length;
		for (int indexA = 0, indexB = 0, lenB = b.length; indexA < lenA
				&& indexB < lenB;) {
			int first = a[indexA];
			int second = b[indexB];

			if (first == second) {
				lastLastResult = lastResult;
				lastResult = COMPARESULT.EQUAL;
				//把相等的值跳过
				while(indexA  < lenA && a[indexA] == first) {
					lastA = indexA++;
				}
				//把相等的值跳过
				while(indexB  < lenB && b[indexB] == first) {
					indexB++;
				}
				continue;
			} else if (first < second) {
				if(lastResult == COMPARESULT.LESS) {
					if (lastLastResult == COMPARESULT.LESS) {
						//将前一个待决定的项放到结果中
						dest.add(a[lastA]);
					}
				} else {
					//这种情况下，一定可以放到结果中
					dest.add(first);
				}

				lastLastResult = lastResult;
				lastResult = COMPARESULT.LESS;
				lastA = indexA++;

			} else {
				//待决定
				lastLastResult = lastResult;
				lastResult = COMPARESULT.THAN;
				lastA = indexA;
				indexB++;

			}
		}

		// 末尾一个要再加以判断
		if (lastA < lenA) {
			if (lastResult == COMPARESULT.THAN
					|| (lastResult == COMPARESULT.LESS && lastLastResult == COMPARESULT.LESS))
				dest.add(a[lastA]);
		}

		for (int indexA = lastA + 1; indexA < lenA; indexA++) {
			dest.add(a[indexA]);
		}
	}

	/**
	 * 求集合的差集 a-b,针对long[] 数组单独写一份，因为java 语言的限制，无法省去
	 * 
	 * @param a
	 * @param b
	 * @param skipSort:是否跳过排序， true 表示 a， b 已经排过序，不需要再排序
	 * @param dest
	 */
	public static void diff(long[] a, long[] b, boolean skipSort,
			Collection<Long> dest) {
		if (!skipSort) {
			Arrays.sort(a);
			Arrays.sort(b);
		}
		// 优化
		if (a[0] > b[b.length - 1] || b[0] > a[a.length - 1]) {
			for (long item : a)
				dest.add(item);
			return;
		}

		COMPARESULT lastOperator = COMPARESULT.UNKNOWN;
		COMPARESULT lastLastOperator = COMPARESULT.UNKNOWN;
		int lastA = -1;
		final int lenA = a.length;
		for (int indexA = 0, indexB = 0, lenB = b.length; indexA < lenA
				&& indexB < lenB;) {
			long first = a[indexA];
			long second = b[indexB];

			if (first == second) {
				lastLastOperator = lastOperator;
				lastOperator = COMPARESULT.EQUAL;
				//把相等的值跳过
				while(indexA  < lenA && a[indexA] == first) {
					lastA = indexA++;
				}
				//把相等的值跳过
				while(indexB  < lenB && b[indexB] == first) {
					indexB++;
				}
				continue;
			} else if (first < second) {
				if(lastOperator == COMPARESULT.LESS) {
					if (lastLastOperator == COMPARESULT.LESS){
						//将前一个待决定的项放到结果中
						dest.add(a[lastA]);
					}
				} else {
					//这种情况下，一定可以放到结果中
					dest.add(first);
				}

				lastLastOperator = lastOperator;
				lastOperator = COMPARESULT.LESS;
				lastA = indexA++;

			} else {
				lastLastOperator = lastOperator;
				lastOperator = COMPARESULT.THAN;
				lastA = indexA;
				indexB++;

			}
		}

		// 末尾一个要再加以判断
		if (lastA < lenA) {
			if (lastOperator == COMPARESULT.THAN
					|| (lastOperator == COMPARESULT.LESS && lastLastOperator == COMPARESULT.LESS))
				dest.add(a[lastA]);
		}

		for (int indexA = lastA + 1; indexA < lenA; indexA++) {
			dest.add(a[indexA]);
		}
	}

	/**
	 * 求集合的交集，就地修改a为包含结果
	 * 
	 * @param a
	 * @param b
	 */
	public static <T extends Comparable<T>> void inplace_intersection(
			Collection<T> a, Collection<T> b) {
		a.retainAll(b);
	}
	
	/**
	 * 求集合的并， 就地修改a为包含结果
	 * @param a
	 * @param b
	 * @param trip： 为  true 时 b 中已经存在于 a的项不要再加入
	 */
	public static <T> void inplace_union(Collection<T> a,
			Collection<T> b, boolean trip) {
		if(trip) {
			Set<T> tmp = new HashSet<T>();
			tmp.addAll(a);
			
			for(T item : b) {
				if(tmp.add(item)) {
					a.add(item);
				}
			}
			tmp.clear();
		} else
			a.addAll(b);
	}

	/**
	 * 求集合的差集，就地修改a为包含结果
	 * 
	 * @param a
	 * @param b
	 */
	public static <T extends Comparable<T>> void inplace_diff(Collection<T> a,
			Collection<T> b) {
		a.removeAll(b);
	}
	
	/**
	 * 合并 a, b: 重复的项保留起来, 就地修改a 为保存的结果
	 * @param a
	 * @param b
	 * @param dest
	 */
	public static <K,V> void inplace_mergelist(Map<K, List<V>> a,
			Map<K, V> b) {
		for(Map.Entry<K, V> entry : b.entrySet()) {
			K key = entry.getKey();
			V value = entry.getValue();
			List<V> old = a.get(key);
			if(old == null) {
				old = new ArrayList<V>(1);
				a.put(key, old);
			}
			old.add(value);
		}
	}
	
	/**
	 * 合并 a, b: 重复的项保留起来
	 * @param a
	 * @param b
	 * @param dest
	 */
	public static <K,V,O> void inplace_mergelist(Map<K, List<V>> a,
			Collection<O> b, MapHelper<K,V,O> helper) {
		for(O entry : b) {
			K key = helper.getKey(entry);
			V value = helper.getValue(entry);
			List<V> old = a.get(key);
			if(old == null) {
				old = new ArrayList<V>(1);
				a.put(key, old);
			}
			old.add(value);
		}
	}
	
	/**
	 * 合并 a, b: 重复的项保留起来
	 * @param a
	 * @param b
	 * @param dest
	 */
	public static <K,V> void inplace_mergeset(Map<K, Set<V>> a,
			Map<K, V> b) {
		for(Map.Entry<K, V> entry : b.entrySet()) {
			K key = entry.getKey();
			V value = entry.getValue();
			Set<V> old = a.get(key);
			if(old == null) {
				old = new HashSet<V>(1, 0.5f);
				a.put(key, old);
			}
			old.add(value);
		}
	}
	
	
	/**
	 * 合并 a, b: 重复的项保留起来
	 * @param a
	 * @param b
	 * @param dest
	 */
	public static <K,V,O> void inplace_mergeset(Map<K, Set<V>> a,
			Collection<O> b, MapHelper<K,V,O> helper) {
		for(O entry : b) {
			K key = helper.getKey(entry);
			V value = helper.getValue(entry);
			Set<V> old = a.get(key);
			if(old == null) {
				old = new HashSet<V>(1,0.5f);
				a.put(key, old);
			}
			old.add(value);
		}
	}

	
	public static void main(String[] args) {
		int[] a = new int[] { 10,  20, 20,30, 30, 40, 50, 50, 60 };
		int[] b = new int[] { 12, 20,  30, 50 };

		List<Integer> left = new ArrayList<Integer>();
		for(int item : a) {
			left.add(item);
		}
		
		List<Integer> right = new ArrayList<Integer>();
		for(int item : b) {
			right.add(item);
		}
		
		List<Integer> result = new ArrayList<Integer>();
		diff(a, b, false, result);
		System.out.println("diff1: " + result);
		
		result.clear();
		diff(left, right, false, result);
		System.out.println("diff2: " + result);
		
		result.clear();
		intersection(a, b, false, result);
		System.out.println("intersection1: " + result);
		
		result.clear();
		intersection(left, right, false, result);
		System.out.println("intersection2: " + result);
		
		result.clear();
		union(a, b, true, result);
		System.out.println("union1: " + result);
		
		result.clear();
		union(left, right, true, result);
		System.out.println("union2: " + result);
		
		
	}
}
