package com.hylanda.mrw.algorithm.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public class GenericsUtils {

	/**   
     * 通过反射,获得定义Class时声明的父类的范型参数的类型.   
     * 如public BookManager extends GenricManager<Book>   
     *   
     * @param clazz The class to introspect   
     * @return the first generic declaration, or <code>Object.class</code> if cannot be determined   
     */  
    public static Class getSuperClassGenricType(Class clazz) {  
        return getSuperClassGenricType(clazz, 0);  
    }  
  
    /**   
     * 通过反射,获得定义Class时声明的父类的范型参数的类型.   
     * 如public BookManager extends GenricManager<Book>   
     *   
     * @param clazz clazz The class to introspect   
     * @param index the Index of the generic ddeclaration,start from 0.   
     */  
    public static Class getSuperClassGenricType(Class clazz, int index) throws IndexOutOfBoundsException {  
  
    	if(clazz == null)
    		return null;
    	
        Type genType = clazz.getGenericSuperclass();  
  
        if (!(genType instanceof ParameterizedType)) { 
        	clazz = clazz.getSuperclass();
        	return getSuperClassGenricType(clazz, index);
        }  
  
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();  
  
        if (index >= params.length || index < 0) {  
            return Object.class;  
        }  
        if (!(params[index] instanceof Class)) {  
            return Object.class;  
        }  
        return (Class) params[index];  
    } 

}
