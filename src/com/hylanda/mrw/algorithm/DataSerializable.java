package com.hylanda.mrw.algorithm;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public interface DataSerializable {
	public void load(DataInput stream) throws IOException;
	public void save(DataOutput stream) throws IOException;
}
