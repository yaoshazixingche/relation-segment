package com.hylanda.mrw.algorithm;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.hylanda.mrw.algorithm.utils.FileLoading;
import com.hylanda.mrw.algorithm.utils.TextFileUtils;

/**
 * 字典类：使用 trie 存放字典，同时保留词到字典的关系
 * 缺点：我们只保留了词id， 没有保存词文本！这样无法从id得到词的文本
 *      如果有需要，只能有使用词典的程序自己解决
 * @author zhouzusheng
 *
 */
public class Dictionary implements DataSerializable {
	private DictItem[] dicts; //词典分类
	private int[][] word_dicts; //从词获取到词典分类
	private int[][] dict_words; //词典分类获取到词
	private DoubleArrayTrie trie; //词组成的 trie 树 
	private Set<Character> unitChars; //年月日等表示单位的单字
	private Map<Character, Integer> charFreqs; //单字的词频

	public DoubleArrayTrie getTrie() {
		return trie;
	}

	public DictItem[] getDicts() {
		return dicts;
	}

	public int[][] getWord_dicts() {
		return word_dicts;
	}

	public int[][] getDict_words() {
		return dict_words;
	}

	public void setDicts(DictItem[] dicts) {
		this.dicts = dicts;
	}

	public void setWord_dicts(int[][] word_dicts) {
		this.word_dicts = word_dicts;
	}

	public void setDict_words(int[][] dict_words) {
		this.dict_words = dict_words;
	}

	public void setTrie(DoubleArrayTrie trie) {
		this.trie = trie;
	}
	
	public int[] getWordDict(int wordid) {
		return word_dicts[wordid];
	}
	
	public int[] getDictWord(int dictid) {
		return dict_words[dictid];
	}
	
	public DictItem getDict(int dictid) {
		return dicts[dictid];
	}

	public void load(File file) throws IOException {
		File units = new File("/home/hadoop/zhangfang/dict/units.dict");
		File chars = new File("/home/hadoop/zhangfang/dict/chars.dict");
		loadUnitFile(units);
		loadCharFreqFile(chars);
		DataInputStream data = new DataInputStream(new FileInputStream(file)); 
		//DataInputStream data = new FSDataInputStream(new BufferedInputStream(f));
		try {
			load(data);
		} finally {
			data.close();
		}
	}

	public void save(String file) throws IOException {
		FileOutputStream f = new FileOutputStream(file);
		DataOutputStream data = new DataOutputStream(
				new BufferedOutputStream(f));
		try {
			save(data);
		} finally {
			data.close();
		}
	}

	@Override
	public void load(DataInput stream) throws IOException {
		int dictscount = stream.readInt();
		dicts = new DictItem[dictscount];
		for (int i = 0; i < dictscount; i++) {
			DictItem item = new DictItem();
			item.load(stream);
			dicts[i] = item;
		}
		
		int word_dicts_count = stream.readInt();
		
		word_dicts = new int[word_dicts_count][];
		for (int i = 0; i < word_dicts_count; i++) {
			int length = stream.readInt();
			int[] info = new int[length];
			for (int k = 0; k < length; k++) {
				info[k] = stream.readInt();
			}
			word_dicts[i] = info;
		}

		int dict_words__count = stream.readInt();
		dict_words = new int[dict_words__count][];
		for (int i = 0; i < dict_words__count; i++) {
			int length = stream.readInt();
			int[] info = new int[length];
			for (int k = 0; k < length; k++) {
				info[k] = stream.readInt();
				//System.out.println(stream.readInt());
			}
			dict_words[i] = info;
		}
		trie = new DoubleArrayTrie();
		trie.load(stream);
		System.out.println("dict load ok!");
	}

	@Override
	public void save(DataOutput stream) throws IOException {
		stream.writeInt(dicts.length);
		for (DictItem item : dicts) {
			item.save(stream);
		}
		stream.writeInt(word_dicts.length);
		for (int[] item : word_dicts) {
			stream.writeInt(item.length);
			for (int value : item) {
				stream.write(value);
			}
		}
		stream.writeInt(dict_words.length);
		for (int[] item : dict_words) {
			stream.writeInt(item.length);
			for (int value : item) {
				stream.write(value);
			}
		}
		trie.save(stream);
	}
	
	public void loadUnitFile(File file) throws IOException {
		unitChars = new HashSet<Character>();
		InputStream Inputstopwords = null;
		if(file.exists()) {
			Inputstopwords = new FileInputStream(file);
			TextFileUtils.load(Inputstopwords, new FileLoading() {
					public void row(String line, int n) {
						if(line.length() != 1) {
							return;
						}
						unitChars.add(line.charAt(0));
					}
				}
			);
			Inputstopwords.close();
		}
	}
	
	public void loadCharFreqFile(File file) throws IOException {
		this.charFreqs = new HashMap<Character, Integer>();
		InputStream Inputstopwords = null;
		if(file.exists()) {
			Inputstopwords = new FileInputStream(file);
			TextFileUtils.load(Inputstopwords, new FileLoading() {
					public void row(String line, int n) {
						String[] w = line.split(" ");
						if(w.length == 2) {
							try {
								charFreqs.put(w[0].charAt(0), (int)(Math.log(Integer.parseInt(w[1]))*100));//字频计算出自由度
							} catch(NumberFormatException e) {
								//eat...
							}
						} else if(w.length == 1){
							charFreqs.put(w[0].charAt(0), -1);
						}
					}
				}
			);
			Inputstopwords.close();
		}
	}

	/**
	 * 最大匹配，返回最长的词的长度
	 * @param sen
	 * @param offset
	 * @return
	 */
	public int maxMatch(char[] sen, int offset) {
		return maxMatch(sen, offset, sen.length, null);
	}

	/**
	 * 最大匹配，返回最长的词的长度
	 * @param sen
	 * @param offset
	 * @param end
	 * @return
	 */
	public int maxMatch(char[] sen, int offset, int end) {
		return trie.maxMatch(sen, offset, end, null);
	}
	
	/**
	 * 最大匹配，返回最长的词的长度, 同时在lens 中存放每个词的长度
	 * @param sen
	 * @param offset
	 * @param end
	 * @return
	 */
	public int maxMatch(char[] sen, int offset, int end, List<Integer> lens) {
		return trie.maxMatch(sen, offset, end, lens);
	}
	
	public int match(char[] sen, int offset, int end, Collection<MatchPattern<Integer>> result) {
		return trie.match(sen, offset, end, result);
	}
	
	public Pattern<Integer> get(String src) {
		return trie.get(src);
	}
	
	Pattern<Integer> get(char[] code, int pos, int end) {
		return trie.get(code, pos, end);
	}

	public boolean isUnit(char d) {
		return unitChars.contains(d);
	}
	
	public int getFreq(char d) {
		Integer r = charFreqs.get(d);
		return r == null ? -1 : r;
	}
}
