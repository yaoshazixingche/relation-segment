package com.hylanda.mrw.algorithm;

import java.io.BufferedInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public class DoubleArrayTrie implements DataSerializable{
	protected int size;
	protected int check[];
	protected int base[];
	
	public DoubleArrayTrie() {
		
	}
	
	public void load(String file) throws FileNotFoundException {
		FileInputStream f = new FileInputStream(file);
		DataInputStream data = new DataInputStream(new BufferedInputStream(f));
		try {
			load(data);
			data.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void load(DataInput stream) throws IOException {
		size = stream.readInt();
		check = new int[size];
		base = new int[size];
		
		for(int i = 0; i < size; i++) {
			check[i] = stream.readInt();
		}
		for(int i = 0; i < size; i++) {
			base[i] = stream.readInt();
		}
	}
	
	@Override
	public void save(DataOutput stream) throws IOException {
		stream.writeInt(size);
		for(int i = 0; i < size; i++) {
			stream.writeInt(check[i]);
		}
		for(int i = 0; i < size; i++) {
			stream.writeInt(base[i]);
		}
	}
	
	
	public  Pattern<Integer> get(String src) {
		return get(src.toCharArray(), 0, src.length());
	}
	
	/**
	 * 查询 以src 开头的所有单词
	 * @param src
	 * @param result
	 * @return
	 */
	public  int getAll(String src, Collection<Pattern<Integer>> result) {
		return getAll(src.toCharArray(), 0, src.length(), result);
	}
	
	public  Pattern<Integer> get(char[] code, int pos, int end) {
		if(end == -1 || end > code.length)
			end = code.length;
		
		if(pos < 0)
			pos = 0;
		else if(pos > end)
			pos = end;
		
		Pattern<Integer> ret = null;
		
		int b = base[0];
		
		char c0;
		for (int i = pos; i < end; i++) {
			c0 = code[i];
			
			int p = b + (int) (c0) + 1;
			if (p < size && b == check[p]) {
				b = base[p];
				p = b;

				int n = base[p];
				if (n < 0) {
					if( (i+1) == end) {
						int id = -n - 1;
						int length = end - pos;
						char[] buffer = new char[length];
						for(int k = 0; k < buffer.length; k++) {
							buffer[k] = code[k + pos];
						}
						ret = new Pattern<Integer>(buffer, id);
						break;
					}
				}

			} else {
				break;
			}
		}
		return ret;
	}
	
	public  int getAll(char[] code, int pos, int end, Collection<Pattern<Integer>> result) {
		if(end == -1 || end > code.length)
			end = code.length;
		if(pos < 0)
			pos = 0;
		else if(pos > end)
			pos = end;
		
		int b = base[0];
		int p = -1;
		char c0;
		boolean found = false;
		for (int i = pos; i < end; i++) {
			c0 = code[i];
			p = b + (int) (c0) + 1;
			if (p < size && b == check[p]) {
				b = base[p];
				p = b;
				int n = base[p];
				if( (i+1) == end) {
					found = true;
					if(n < 0) {
						int id = -n -1;
						int length  = i - pos +1;
						char[] buffer = new char[length];
						for(int k = 0; k < buffer.length; k++) {
							buffer[k] = code[k + pos];
						}
						Pattern<Integer> pat = new Pattern<Integer>(buffer, id);
						result.add(pat);
					}
				}
			} else {
				break;
			}
		}
		
		if(found) {
			getAllChild(b, new String(code, pos, end), result);
		}
		
		return result.size();
	}
	
	
	
	public int maxMatch(char[] code, int pos, int end, Collection<Integer> lens) {
		if(end == -1 || end > code.length)
			end = code.length;
		
		if(pos < 0)
			pos = 0;
		else if(pos > end)
			pos = end;
		
		int ret = -1;
		
		int b = base[0];
		char c0;
		for (int i = pos; i < end; i++) {
			c0 = code[i];
			
			int p = b + (int) (c0) + 1;
			if (p < size && b == check[p]) {
				b = base[p];
				p = b;

				int n = base[p];
				if (n < 0) {
					ret = i - pos + 1;
					if(lens != null) {
						lens.add(ret);
					}
				}

			} else {
				break;
			}
		}
		return ret;
	}
	
	public int match(char[] code, int pos, int end,
			Collection<MatchPattern<Integer>> resultPattern) {
		int b = base[0];
		if(end < 0)
			end = code.length;
		
		int ret = -1;
		
		char c0;
		for (int i = pos; i < end; i++) {
			c0 = code[i];
			
			int p = b + (int) (c0) + 1;
			if (p < size && b == check[p]) {
				b = base[p];
				p = b;

				int n = base[p];
				if (n < 0) {
					int id = -n - 1;
					int length =  i - pos + 1;
					ret = length;
					
					char[] buffer = new char[length];
					for(int k = 0; k < buffer.length; k++) {
						buffer[k] = code[k + pos];
					}
					Pattern<Integer> pat = new Pattern<Integer>(buffer, id);
					resultPattern.add(new MatchPattern<Integer>(pat, pos, buffer.length));
				}

			} else {
				break;
			}
		}
		return ret;
	}
	
	private void getAllChild(int b, String prev, Collection<Pattern<Integer>> result) {
		int max = (int)Character.MAX_VALUE;
		
		for(int i = 1;  i < max; i++) {
			int p = b + i+ 1;
			if( p >= size)
				break;
			
			if(check[p] == b) {
				String strItem = prev + (char)i;
				int b2 = base[p];
				p = b2;
				int n = base[p];
				if(n < 0) {
					int id = -n -1;
					CharBuffer buffer = CharBuffer.valueOf(strItem);
					Pattern<Integer> pat = new Pattern<Integer>(buffer, id);
					result.add(pat);
				}
				getAllChild(b2, strItem, result);
			}
			
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		DoubleArrayTrie t = new DoubleArrayTrie();
		t.load("dict/trie.dat");
		
		String source = "济南白癜风的治疗方法haha";
		char[] chars = source.toCharArray();
		Set<MatchPattern<Integer>> result = new TreeSet<MatchPattern<Integer>>();
		t.match(chars, 0, chars.length, result);
		for(MatchPattern<Integer> item : result) {
			System.out.println(item);
		}
	}

	
}
