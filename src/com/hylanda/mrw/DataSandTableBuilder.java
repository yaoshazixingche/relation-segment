package com.hylanda.mrw;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.hadoop.io.FloatWritable;

import com.hylanda.mrw.algorithm.Dictionary;
import com.hylanda.mrw.algorithm.Pattern;
import com.hylanda.mrw.algorithm.utils.CollectionUtils;
import com.hylanda.mrw.algorithm.utils.FileLoading;
import com.hylanda.mrw.algorithm.utils.TextFileUtils;

public class DataSandTableBuilder {
	private Dictionary dict;
	private String inputFile;
	private int windowSize = 10;

	private TreeMap<String, FloatWritable> map = new TreeMap<String, FloatWritable>();

	public DataSandTableBuilder() {

	}

	public Dictionary getDict() {
		return dict;
	}

	public void setDict(Dictionary dict) {
		this.dict = dict;
	}

	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public int getWindowSize() {
		return windowSize;
	}

	public void setWindowSize(int windowSize) {
		this.windowSize = windowSize;
	}

	public TreeMap<String, FloatWritable> getMap() {
		return map;
	}

	public boolean build() throws IOException {
		if (dict == null || inputFile == null)
			return false;
		map.clear();

		TextFileUtils.load(inputFile, "GBK", new FileLoading() {

			@Override
			public void row(String line, int n) {
				buildLine(line);
			}

		});

		return true;

	}

	public void save(String file) throws IOException {
		// File relationFile = new File(file);
		OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(
				file), "UTF-8");
		BufferedWriter output = new BufferedWriter(osw);
		for (Map.Entry<String, FloatWritable> entry : map.entrySet()) {
			output.write(entry.getKey() + " " + entry.getValue() + "\n");
		}
		output.close();
	}

	private void buildLine(String line) {
		String[] words = line.split(" ");
		for (int i = 0; i < words.length; i++) {
			String wordA = words[i];
			Pattern<Integer> patA = dict.get(wordA);

			for (int j = 1; j <= windowSize && (i + j) < words.length; j++) {
				String wordB = words[i + j];
				Pattern<Integer> patB = dict.get(wordB);

				FloatWritable value = value(patA, patB, j - i);

				String keyA, keyB;
				if (patA == null) {
					keyA = "-1 " + wordA;
				} else {
					keyA = patA.getValue() + " " + wordA;
				}

				if (patB == null) {
					keyB = "-1 " + wordB;
				} else {
					keyB = patB.getValue() + " " + wordB;
				}

				String key1 = keyA + " " + keyB;
				String key2 = keyB + " " + keyA;

				FloatWritable value1 = map.get(key1);
				FloatWritable value2 = map.get(key2);
				if (value1 == null || value2 == null) {
					map.put(key1, value);
					map.put(key2, value);
				} else {
					map.put(key1, new FloatWritable(value1.get()+value.get()));
					map.put(key2, new FloatWritable(value1.get()+value.get()));
				}
			}
		}
	}


	public FloatWritable value(Pattern<Integer> patA, Pattern<Integer> patB,int distance) {
		float factor = 1f;
		//不是词典词， 权重减半
		/*if (patA == null || patB == null) {
			factor = 0.5f;
		} else {
			int dictsA[] = dict.getWordDict(patA.getValue());
			//System.out.println("patA:"+patA.getValue());
			int dictsB[] = dict.getWordDict(patB.getValue());
			//System.out.println("patB:"+patB.getValue());

			//这个地方可以优化一下，目前是判断交集不空，即有共同的词典分类时将权重 * 2.5
			//优化方案是提供一个IsIntersection 函数
			if(CollectionUtils.isIntersection(dictsA, dictsB, true))
				factor = 2.5f;
		}*/

		if (distance <= 1)
			return new FloatWritable(factor);
		else if (distance <= 3)
			return  new FloatWritable((float) (0.6*factor));
		else
			return  new FloatWritable((float) (0.2*factor));
	}
}
