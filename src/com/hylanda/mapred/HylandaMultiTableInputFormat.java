package com.hylanda.mapred;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.hbase.ClusterStatus;
import org.apache.hadoop.hbase.HRegionInfo;
import org.apache.hadoop.hbase.HServerLoad;
import org.apache.hadoop.hbase.HServerLoad.RegionLoad;
import org.apache.hadoop.hbase.ServerName;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.MultiTableInputFormat;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.Pair;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HylandaMultiTableInputFormat extends MultiTableInputFormat {
	private static Logger LOG = LoggerFactory
			.getLogger(HylandaMultiTableInputFormat.class);

	@SuppressWarnings("deprecation")
	@Override
	public List<InputSplit> getSplits(JobContext context) throws IOException {
		if (getScans().isEmpty()) {
			throw new IOException("No scans were provided.");
		}
		List<InputSplit> splits = new ArrayList<InputSplit>();
		/*
		 * 获取region大小begin,新增
		 */
		Map<String, RegionLoad> regionsNameToLoad = new HashMap<String, RegionLoad>();
		try {
			final HBaseAdmin admin = new HBaseAdmin(context.getConfiguration());
			ClusterStatus clusterStatus = admin.getClusterStatus();
			for (ServerName serverName : clusterStatus.getServers()) {
				HServerLoad load = clusterStatus.getLoad(serverName);
				Map<byte[], RegionLoad> regionsLoad = load.getRegionsLoad();
				for (Map.Entry<byte[], RegionLoad> entry : regionsLoad
						.entrySet()) {
					RegionLoad regionLoad = entry.getValue();
					regionsNameToLoad.put(
							Bytes.toStringBinary(regionLoad.getName()),
							regionLoad);
					LOG.debug("regionLoad.getNameAsString()="
							+ Bytes.toStringBinary(regionLoad.getName())
							+ " getStorefileSizeMB="
							+ regionLoad.getStorefileSizeMB());
				}
			}
		} catch (IOException e) {
			throw new RuntimeException("Failed while fetching cluster load:"
					+ e.getMessage(), e);
		}
		// 获取region大小end
		// 获取分裂标准
		int splite_size = context.getConfiguration().getInt(
				"SpliteRegionSizeMB", 1024);
		// 获取是否配置按照大小分裂
		boolean IsSpliteRegion = context.getConfiguration().getBoolean(
				"IsSpliteRegion", false);
		for (Scan scan : getScans()) {
			byte[] tableName = scan.getAttribute("scan.attributes.table.name");
			if (tableName == null)
				throw new IOException("A scan object did not have a table name");
			HTable table = new HTable(context.getConfiguration(), tableName);
			Pair<?, ?> keys = table.getStartEndKeys();
			if ((keys == null) || (keys.getFirst() == null)
					|| (((byte[][]) keys.getFirst()).length == 0)) {
				throw new IOException(
						"Expecting at least one region for table : "
								+ Bytes.toString(tableName));
			}

			byte[] startRow = scan.getStartRow();
			byte[] stopRow = scan.getStopRow();
			LOG.info("startRow = " + printRowKey(startRow));
			LOG.info("stopRow = " + printRowKey(stopRow));
			Set<HRegionInfo> regionibfos = table.getRegionsInfo().keySet();

			for (HRegionInfo hRegionInfo : regionibfos) {
				if (!includeRegionInSplit(hRegionInfo.getStartKey(),
						hRegionInfo.getEndKey())) {
					continue;
				}
				String regionLocation = table.getRegionLocation(
						hRegionInfo.getStartKey(), false).getHostname();

				if (((startRow.length != 0)
						&& (hRegionInfo.getEndKey().length != 0) && (Bytes
						.compareTo(startRow, hRegionInfo.getEndKey()) >= 0))
						|| ((stopRow.length != 0) && (Bytes.compareTo(stopRow,
								hRegionInfo.getStartKey()) <= 0))) {
					continue;
				}
				byte[] splitStart = (startRow.length == 0)
						|| (Bytes
								.compareTo(hRegionInfo.getStartKey(), startRow) >= 0) ? hRegionInfo
						.getStartKey() : startRow;

				byte[] splitStop = ((stopRow.length == 0) || (Bytes.compareTo(
						hRegionInfo.getEndKey(), stopRow) <= 0))
						&& (hRegionInfo.getEndKey().length > 0) ? hRegionInfo
						.getEndKey() : stopRow;

				boolean originalFlag = true;
				// 新增逻辑
				if (regionsNameToLoad.containsKey(Bytes
						.toStringBinary(hRegionInfo.getRegionName()))) {
					int StoreSize = regionsNameToLoad.get(
							Bytes.toStringBinary(hRegionInfo.getRegionName()))
							.getStorefileSizeMB();
					LOG.info("Regionname = "
							+ Bytes.toStringBinary(hRegionInfo.getRegionName())
							+ " and StoreSize = " + StoreSize);
					// 判断region大小是否大于给定:单位为M,startkey 不会为空
					if ((StoreSize > splite_size) && IsSpliteRegion
							&& (0 != splitStop.length)) {
						// 置标志位为FALSE
						originalFlag = false;
						// 计算分裂数量
						BigInteger splitsize = BigInteger.valueOf(StoreSize)
								.divide(BigInteger.valueOf(splite_size));
						// 开始计算分裂key,split自动对splitsize加1
						byte[][] splitkeys = Bytes.split(splitStart, splitStop,
								splitsize.intValue());
						for (int i = 0; i < splitkeys.length - 1; i++) {
							InputSplit split = new TableSplit(
									table.getTableName(), scan, splitkeys[i],
									splitkeys[i + 1], regionLocation);
							splits.add(split);
							LOG.info("splited by size Start = "
									+ printRowKey(splitkeys[i]));
							LOG.info("splited by size Stop = "
									+ printRowKey(splitkeys[i + 1]));
							LOG.info("getSplits by size : split -> "
									+ splits.size() + " -> " + split);
						}
					}
				}
				if (originalFlag) {
					// 原程序逻辑
					LOG.info("splitStart = " + printRowKey(splitStart));
					LOG.info("splitStop = " + printRowKey(splitStop));
					InputSplit split = new TableSplit(table.getTableName(),
							scan, splitStart, splitStop, regionLocation);
					splits.add(split);
					LOG.info("getSplits: split -> " + splits.size() + " -> "
							+ split);
				}
			}
			table.close();
		}
		return splits;
	}

	// 将指定byte数组以16进制的形式打印到控制台
	public static String Bytes2HexString(byte[] b) {
		String ret = "";
		for (int i = 0; i < b.length; i++) {
			String hex = Integer.toHexString(b[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			ret += hex.toUpperCase();
		}
		return ret;
	}

	public static String printRowKey(byte[] rowkey) {
		if (null == rowkey) {
			LOG.info("rowkey is null");
			return null;
		} else if (0 == rowkey.length) {
			LOG.info("rowkey length is 0");
			return "";
		}
		String strrowkey = new String();
		strrowkey += Bytes2HexString(Bytes.head(rowkey, 8)) + "_";
		strrowkey += Bytes2HexString(Bytes.tail(Bytes.head(rowkey, 16), 8))
				+ "_";
		strrowkey += Bytes.toString(rowkey, 16, 1) + "_";
		strrowkey += Bytes2HexString(Bytes.tail(rowkey, 8));
		return strrowkey;
	}

	public static void main(String[] args) {
		byte[] start = new byte[0];
		byte[] end = { 1, 2, 3, 4, 5 };
		byte[][] xx = Bytes.split(start, end, 1);
		for (byte[] x : xx) {
			System.out.println(x.toString());
		}

	}
}